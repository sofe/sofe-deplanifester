module Models.Manifest (..) where

import Dict exposing (Dict)

type alias Manifest = Dict String String
